--inser operator
INSERT INTO "operator" (name,created_at,updated_at) values ('Andi Sutisna',now(),now());
insert into "operator" (name,created_at,updated_at) values ('Bambang Yudika',now(),now());
insert into "operator" (name,created_at,updated_at) values ('Cika Lestari',now(),now());
insert into "operator" (name,created_at,updated_at) values ('Deni Iskandar',now(),now());
insert into "operator" (name,created_at,updated_at) values ('Mulan Gemilang',now(),now());

--inser product type
insert into product_type (name_product,created_at,update_at) values ('Handphone',now(),now());
insert into product_type (name_product,created_at,update_at) values ('Aksesoris Handphone',now(),now());
insert into product_type (name_product,created_at,update_at) values ('Pulsa',now(),now());

--insert products
insert into products (id_product_type, id_product_desc, id_operator, code, name, status, created_at,update_at) values (1,1,3,'HP001','Samsung',100,now(),now());
insert into products (id_product_type, id_product_desc, id_operator, code, name, status, created_at,update_at) values (1,1,3,'HP002','Iphone',100,now(),now());

insert into products (id_product_type, id_product_desc, id_operator, code, name, status, created_at,update_at) values (2,2,1,'AH001','Tempered Glass',100,now(),now());
insert into products (id_product_type, id_product_desc, id_operator, code, name, status, created_at,update_at) values (2,2,1,'AH002','Powerbank',100,now(),now());
insert into products (id_product_type, id_product_desc, id_operator, code, name, status, created_at,update_at) values (2,2,1,'AH003','Earphone TWS Beasus',100,now(),now());

insert into products (id_product_type, id_product_desc, id_operator, code, name, status, created_at,update_at) values (3,3,4,'PL001','Pulsa Senilai 25.000',100,now(),now());
insert into products (id_product_type, id_product_desc, id_operator, code, name, status, created_at,update_at) values (3,3,4,'PL002','Pulsa Senilai 50.000',100,now(),now());
insert into products (id_product_type, id_product_desc, id_operator, code, name, status, created_at,update_at) values (3,3,4,'PL002','Pulsa Senilai 100.000',100,now(),now());

--insert product description 
insert into product_desc  (description,created_at,update_at) values ('Segala macam perangkat handphone',now(),now());
insert into product_desc  (description,created_at,update_at) values ('Kebutuhan aksesoris untuk handphone',now(),now());
insert into product_desc  (description,created_at,update_at) values ('Menjual pulsa elektrik',now(),now());
select * from product_desc;
--insert payment method
INSERT INTO payment_methods (name, created_at, update_at) VALUES ('Tunai',now(),now());
INSERT INTO payment_methods (name, created_at, update_at) VALUES ('Transer Bank',now(),now());
INSERT INTO payment_methods (name, created_at, update_at) VALUES ('DOKU Payment',now(),now());


--inser user
INSERT INTO "user" (name,address,phone_number,email,date_of_birth,gender,status,created_at) VALUES ('Jaka Tingkir', 'Cibinong','0812736128731','jakatingkir@mail.com','1990-05-09','Laki-laki','1',now());
INSERT INTO "user" (name,address,phone_number,email,date_of_birth,gender,status,created_at) VALUES   ('Jennie Indri Sari', 'Cibubur','081223241256','jennieindri95@mail.com','1995-02-18','Perempuan','0',now());
INSERT INTO "user" (name,address,phone_number,email,date_of_birth,gender,status,created_at) VALUES ('Zaelani Maulana', 'Sukatani','089512582313','zae007@mail.com','1997-07-07','Laki-laki','0',now());
INSERT INTO "user" (name,address,phone_number,email,date_of_birth,gender,status,created_at) VALUES ('Mochtar Mahmud', 'Tapos','081588823193','mochtarmochtir@mail.com','1989-10-21','Laki-laki','1',now());
INSERT INTO "user" (name,address,phone_number,email,date_of_birth,gender,status,created_at) VALUES ('Tari Indah', 'Citayem','087868623823','tariindah12@mail.com','1999-12-12','Perempuan','0',now());

--insert transaction
INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (1,3,'Lunas',3,5350000,now());
INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (2,2,'Lunas',3,10350000,now());
INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (3,3,'Lunas',3,450000,now());
INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (4,1,'Lunas',3,451000,now());
INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (5,1,'Lunas',3,451000,now());

INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (1,3,'Lunas',3,101000,now());
INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (1,3,'Lunas',1,51000,now());

INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (2,2,'Lunas',1,101000,now());
INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (2,2,'Lunas',1,51000,now());

INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (3,3,'Lunas',1,1000000,now());
INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (3,3,'Lunas',1,100000,now());

INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (4,1,'Lunas',1,5000000,now());
INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (4,1,'Lunas',1,100000,now());

INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (5,1,'Lunas',1,250000,now());
INSERT INTO "transaction" (id_user,id_payment_methods,status,total_qty,total_price,created_at) VALUES (5,1,'Lunas',1,250000,now());

UPDATE "transaction" SET total_qty = 3, total_price = 303000 WHERE id = 6;
UPDATE "transaction" SET total_qty = 3, total_price = 153000 WHERE id = 7;
UPDATE "transaction" SET total_qty = 3, total_price = 303000 WHERE id = 8;
UPDATE "transaction" SET total_qty = 3, total_price = 153000 WHERE id = 9;
UPDATE "transaction" SET total_qty = 3, total_price = 30000000 WHERE id = 10;
UPDATE "transaction" SET total_qty = 3, total_price = 300000 WHERE id = 11;
UPDATE "transaction" SET total_qty = 3, total_price = 15000000 WHERE id = 12;
UPDATE "transaction" SET total_qty = 3, total_price = 300000 WHERE id = 13;
UPDATE "transaction" SET total_qty = 3, total_price = 750000 WHERE id = 14;
UPDATE "transaction" SET total_qty = 3, total_price = 750000 WHERE id = 15;


--insert transaction detail
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (1,1,'Lunas','1',5000000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (1,3,'Lunas','1',100000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (1,5,'Lunas','1',250000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (2,2,'Lunas','1',10000000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (2,3,'Lunas','1',100000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (2,4,'Lunas','1',250000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (3,4,'Lunas','1',250000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (3,3,'Lunas','1',100000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (3,3,'Lunas','1',100000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (4,4,'Lunas','1',250000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (4,3,'Lunas','1',100000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (4,8,'Lunas','1',101000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (5,4,'Lunas','1',250000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (5,3,'Lunas','1',100000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (5,8,'Lunas','1',101000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (6,8,'Lunas','1',101000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (6,8,'Lunas','1',101000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (6,8,'Lunas','1',101000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (7,7,'Lunas','1',51000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (7,7,'Lunas','1',51000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (7,7,'Lunas','1',51000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (8,8,'Lunas','1',101000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (8,8,'Lunas','1',101000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (8,8,'Lunas','1',101000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (9,7,'Lunas','1',51000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (9,7,'Lunas','1',51000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (9,7,'Lunas','1',51000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (10,2,'Lunas','1',10000000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (10,2,'Lunas','1',10000000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (10,2,'Lunas','1',10000000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (11,3,'Lunas','1',100000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (11,3,'Lunas','1',100000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (11,3,'Lunas','1',100000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (12,1,'Lunas','1',5000000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (12,1,'Lunas','1',5000000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (12,1,'Lunas','1',5000000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (13,3,'Lunas','1',100000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (13,3,'Lunas','1',100000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (13,3,'Lunas','1',100000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (14,5,'Lunas','1',250000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (14,5,'Lunas','1',250000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (14,5,'Lunas','1',250000, now());

INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (15,4,'Lunas','1',250000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (15,4,'Lunas','1',250000, now());
INSERT INTO transaction_details (id_transaction, id_products, status, qty, price, created_at) VALUES (15,4,'Lunas','1',250000, now());

--select gender laki laki
SELECT * FROM "user" WHERE gender = 'Laki-laki'; 

--select product id=3
SELECT * FROM products WHERE id=3;

--select data pelanggan yang created_at 7 hari kebelakang dan memiliki nama mengandung huruf 'a'
SELECT * FROM "user" WHERE created_at >= current_date - INTERVAL '7 days' AND name LIKE '%a%';

--select count data perempuan
SELECT count(*) FROM "user" WHERE gender = 'Perempuan';

--select data pelanggan sesuai abjad
SELECT * FROM "user" ORDER BY name ASC ;

--select 5 data product
SELECT * FROM products LIMIT 5;

--update data product id=1 dengan nama "product dummy"
UPDATE products SET "name" = 'product dummy' WHERE id=1;

--update data transaction detail id=1 dengan qty=3
UPDATE transaction_details SET qty = 3 WHERE id=1;

--delete data product dengan id=1
DELETE FROM products WHERE id=1;

--delete data product dengan id_product_type=1
DELETE FROM products WHERE id_product_type = 1;

--gabungkan data transaksi user id=1 dan id=2
SELECT * FROM "transaction" WHERE id_user = 1 UNION SELECT * FROM "transaction" WHERE id_user =2;

--tampilkan jumlah harga transaksi untuk user id=1
SELECT sum(total_price) FROM "transaction" WHERE id_user =1;

--tampilkan total transaksi dengan product type = 2
SELECT sum(t.id) FROM "transaction" t LEFT JOIN transaction_details td ON t.id = td.id WHERE t.id IN (SELECT td.id_transaction  FROM products p LEFT JOIN transaction_details td ON p.id = td .id WHERE  p.id_product_type = 2);

--tampilkan semua transaction dan product type yang saling berhubungan
SELECT * 
FROM "transaction" t 
LEFT JOIN transaction_details td 
ON t.id = td.id 
WHERE t.id IN 
	(SELECT p.id_product_type  
	 FROM products p 
	 LEFT JOIN transaction_details td 
	 ON p.id = td .id);

--function delete transaksi maka transaction detail yang terhubung dengan id transaction terhapus


CREATE OR REPLACE FUNCTION delete_all_transaction_details() 
RETURNS TRIGGER AS $$
BEGIN
    DELETE FROM transaction_details WHERE id_transaction = OLD.id;
    RETURN OLD;
END $$ LANGUAGE 'plpgsql';

CREATE TRIGGER delete_transaction_details 
BEFORE DELETE ON transaction
FOR EACH ROW 
EXECUTE PROCEDURE delete_all_transaction_details();


DELETE FROM transaction WHERE id=1;
SELECT * FROM transaction_details ;

--function delete transaksi details maka total qty pada transaction akan berkurang
CREATE OR REPLACE FUNCTION update_total_qty() 
RETURNS TRIGGER AS $$
BEGIN 
	UPDATE transaction
    SET total_qty  = total_qty  - OLD.qty 
    WHERE id = OLD.id_transaction;
return NEW;
END $$ LANGUAGE 'plpgsql';

CREATE TRIGGER delete_transaction_details
    BEFORE DELETE ON transaction_details
    FOR EACH ROW
    EXECUTE PROCEDURE update_total_qty();
DELETE FROM transaction_details WHERE id=1;
SELECT * FROM "transaction";
--tampilkan data products yang tidak ada pada data transaction details dengan sub query
SELECT *
FROM products
WHERE id 
IN (
SELECT id_products  FROM transaction_details GROUP BY id_products
);

